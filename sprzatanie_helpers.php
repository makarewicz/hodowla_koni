<?
require_once('common.php');
require_once('pracownik_helpers.php');
require_once('box_helpers.php');

function delete_sprzatanie_from_post() {
    $dbconn = connect_to_db();
    pg_delete($dbconn, 'mydb.sprzatanie', array('idsprzatanie' => $_POST['idsprzatanie']));
}

function insert_sprzatanie_from_post() {
    $dbconn = connect_to_db();
    pg_insert($dbconn, 'mydb.sprzatanie', $_POST);
}

function update_sprzatanie_from_post() {
    $dbconn = connect_to_db();
    pg_update($dbconn, 'mydb.sprzatanie', $_POST, array('idsprzatanie' => $_POST['idsprzatanie']));
}

function get_sprzatanie_url($id, $action='get') {
    return 'sprzatanie.php?id=' . $id . '&action=' . $action;
}

function get_sprzatanie_link($id, $action='get', $text = NULL) {
    if (is_null($text)) {
        $text = $id;
    }
    return '<a href=\'' . get_sprzatanie_url($id, $action) . '\'>' . $text . '</a>';
}

function query_sprzatanie_all() {
    return 'SELECT * FROM mydb.sprzatanie';
}

function query_sprzatanie_by_id($id) {
    return 'SELECT * FROM mydb.sprzatanie WHERE idsprzatanie=' . $id;
}

function display_sprzatanie_table() {
    $query = query_sprzatanie_all();
    display_query_result($query, 'get_sprzatanie_link');
}

function get_sprzatanie_list($id, $name='') {
    $query = 'SELECT idsprzatanie FROM mydb.sprzatanie';
    $result = get_query_result($query);
    $drop_list = '<select name=\'' . $name . '\'>';
    $drop_list .= '<option value=\'\'> N/A </option>';
    while ($row = pg_fetch_row($result)) {
        $drop_list .= '<option value=\'' . $row[0] . '\'>' . $row[0] . '</option>';
    }
    $drop_list .= '</select>';
    return $drop_list;

}

function display_sprzatanie_entry($id, $for_edit) {
    if ($for_edit) {
        $map_dict['dozorca_id'] = 'get_pracownik_list';
        $map_dict['box_id'] = 'get_box_list';
    } else {
        $map_dict['dozorca_id'] = 'get_pracownik_link';
        $map_dict['dozorca_id'] = 'get_box_link';
    }
    $query = query_sprzatanie_by_id($id);
    display_entry($query, $map_dict, $for_edit);
}

?>
