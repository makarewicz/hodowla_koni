<?
require_once('common.php');
require_once('kon_helpers.php');
require_once('pracownik_helpers.php');

function delete_zabieg_from_post() {
    $dbconn = connect_to_db();
    pg_delete($dbconn, 'mydb.zabieg', array('idzabieg' => $_POST['idzabieg']));
}

function insert_zabieg_from_post() {
    $dbconn = connect_to_db();
    pg_insert($dbconn, 'mydb.zabieg', $_POST);
}

function update_zabieg_from_post() {
    $dbconn = connect_to_db();
    pg_update($dbconn, 'mydb.zabieg', $_POST, array('idzabieg' => $_POST['idzabieg']));
}

function get_zabieg_url($id, $action='get') {
    return 'zabieg.php?id=' . $id . '&action=' . $action;
}

function get_zabieg_link($id, $action='get', $text = NULL) {
    if (is_null($text)) {
        $text = $id;
    }
    return '<a href=\'' . get_zabieg_url($id, $action) . '\'>' . $text . '</a>';
}

function query_zabieg_all() {
    return 'SELECT * FROM mydb.zabieg';
}

function query_zabieg_by_id($id) {
    return 'SELECT * FROM mydb.zabieg WHERE idzabieg=' . $id;
}

function display_zabieg_table() {
    $query = query_zabieg_all();
    display_query_result($query, 'get_zabieg_link');
}

function get_zabieg_list($id, $name='') {
    $query = 'SELECT idzabieg, imie FROM mydb.zabieg';
    $result = get_query_result($query);
    $drop_list = '<select name=\'' . $name . '\'>';
    $drop_list .= '<option value=\'\'> N/A </option>';
    while ($row = pg_fetch_row($result)) {
        $drop_list .= '<option value=\'' . $row[0] . '\'>' . $row[1] . '</option>';
    }
    $drop_list .= '</select>';
    return $drop_list;

}

function display_zabieg_entry($id, $for_edit) {
    if ($for_edit) {
        $map_dict['kon_id'] = 'get_kon_list';
        $map_dict['weterynarz_id'] = 'get_pracownik_list';
    } else {
        $map_dict['kon_id'] = 'get_kon_link';
        $map_dict['weterynarz_id'] = 'get_pracownik_link';
    }
    $query = query_zabieg_by_id($id);
    display_entry($query, $map_dict, $for_edit);
}

?>
