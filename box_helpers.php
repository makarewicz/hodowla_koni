<?
require_once('common.php');
require_once('kon_helpers.php');

function delete_box_from_post() {
    $dbconn = connect_to_db();
    pg_delete($dbconn, 'mydb.box', array('idbox' => $_POST['idbox']));
}

function insert_box_from_post() {
    $dbconn = connect_to_db();
    pg_insert($dbconn, 'mydb.box', $_POST);
}

function update_box_from_post() {
    $dbconn = connect_to_db();
    pg_update($dbconn, 'mydb.box', $_POST, array('idbox' => $_POST['idbox']));
}

function get_box_url($id, $action='get') {
    return 'box.php?id=' . $id . '&action=' . $action;
}

function get_box_link($id, $action='get', $text = NULL) {
    if (is_null($text)) {
        $text = $id;
    }
    return '<a href=\'' . get_box_url($id, $action) . '\'>' . $text . '</a>';
}

function query_box_all() {
    return 'SELECT * FROM mydb.box';
}

function query_box_by_id($id) {
    return 'SELECT * FROM mydb.box WHERE idbox=' . $id;
}

function display_box_table() {
    $query = query_box_all();
    display_query_result($query, 'get_box_link');
}

function get_box_list($id, $name='') {
    $query = 'SELECT idbox FROM mydb.box';
    $result = get_query_result($query);
    $drop_list = '<select name=\'' . $name . '\'>';
    $drop_list .= '<option value=\'\'> N/A </option>';
    while ($row = pg_fetch_row($result)) {
        $drop_list .= '<option value=\'' . $row[0] . '\'>' . $row[0] . '</option>';
    }
    $drop_list .= '</select>';
    return $drop_list;

}

function display_box_entry($id, $for_edit) {
    if ($for_edit) {
        $map_dict['kon_id'] = 'get_kon_list';
    } else {
        $map_dict['kon_id'] = 'get_kon_link';
    }
    $query = query_box_by_id($id);
    display_entry($query, $map_dict, $for_edit);
}

?>
