<?php
require_once('kon_helpers.php');
function connect_to_db() {
    $host = 'localhost';
    $port = '5433';
    $database = 'hodowla';
    $user = 'postgres';
    $password = 'jamjest3';

    $connect_string = 'host=' . $host . ' port=' . $port . ' dbname=' . $database . 
        ' user=' . $user . ' password=' . $password;
    $link = pg_connect($connect_string);
    if (!$link) {
        die('Error: Could not connect: ' . pg_last_error());
    }
    return $link;
}

function get_query_result($query) {
    connect_to_db();
    return pg_query($query);
}

function display_entry($query, $map_dict=NULL, $editable=false) {
    if (is_null($map_dict)) {
        $map_dict = array();
    }
    $result = get_query_result($query);
    $count = pg_num_fields($result);
    $row = pg_fetch_row($result);
    if (!$row && !$editable) {
        die('Error: Requested entry doesn\'t exist.');
    }
    $i = 0;
    echo '<table>';
    while ($i < $count) {
        if (!$row) {
            $c_row = '';
        } else {
            $c_row = current($row);
            next($row);
        }
        $field_name = pg_field_name($result, $i);
        if ($i == 0) {
            if($c_row != '') {
                echo '<input type=\'hidden\' name=\'' . $field_name .
                    '\' value=\'' . $c_row . '\' />';
            }
        } else {
            if (isset($map_dict[$field_name])) {
                if($editable) {
                    $c_row = call_user_func($map_dict[$field_name], $c_row, $field_name);
                } else {
                    $c_row = call_user_func($map_dict[$field_name], $c_row);
                }
            } else if ($editable) {
                $c_row =  '<input type=\'text\' name=\'' . $field_name .
                    '\' value=\'' . $c_row . '\' />';
            }
            echo '<tr>';
            echo '<td>' . $field_name . '</td><td>' . $c_row . '</td>';
            echo '</tr>';
        }
        $i = $i + 1;
    }
    echo '</table>';
}

function display_query_result($query, $link_func=NULL) {

    $result = get_query_result($query);

    // Displaying column names.
    $i = 0;
    echo '<table><tr>';
    while ($i < pg_num_fields($result)) {
        $field_name = pg_field_name($result, $i);
        echo '<td>' . $field_name . '</td>';
        $i = $i + 1;
    }
    if (!is_null($link_func)) {
        echo '<td></td><td></td>';
    }
    echo '</tr>';

    // Displaying rows.
    $i = 0;
    while ($row = pg_fetch_row($result)) {
        echo '<tr>';
        $count = count($row);
        $j = 0;
        $id = 0;
        while ($j < $count) {
            $c_row = current($row);
            if($j == 0) {
                $id = $c_row;
            }
            echo '<td>' . $c_row . '</td>';
            next($row);
            $j = $j + 1;
        }
        if (!is_null($link_func)) {
            echo '<td>' . call_user_func($link_func, $id, 'get', 'Pokaz') . '</td><td>' . call_user_func($link_func, $id, 'edit', 'Edytuj') . '</td>';
        }
        echo '</tr>';
        $i = $i + 1;
    }
    echo '</table>';
    pg_free_result($result);
}

function show_table($table_name) {
    display_query_result('SELECT * FROM ' . $table_name . ';');
}

?>
