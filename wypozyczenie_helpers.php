<?
require_once('common.php');
require_once('kon_helpers.php');
require_once('klient_helpers.php');

function delete_wypozyczenie_from_post() {
    $dbconn = connect_to_db();
    pg_delete($dbconn, 'mydb.wypozyczenie', array('idwypozyczenie' => $_POST['idwypozyczenie']));
}

function insert_wypozyczenie_from_post() {
    $dbconn = connect_to_db();
    pg_insert($dbconn, 'mydb.wypozyczenie', $_POST);
}

function update_wypozyczenie_from_post() {
    $dbconn = connect_to_db();
    pg_update($dbconn, 'mydb.wypozyczenie', $_POST, array('idwypozyczenie' => $_POST['idwypozyczenie']));
}

function get_wypozyczenie_url($id, $action='get') {
    return 'wypozyczenie.php?id=' . $id . '&action=' . $action;
}

function get_wypozyczenie_link($id, $action='get', $text = NULL) {
    if (is_null($text)) {
        $text = $id;
    }
    return '<a href=\'' . get_wypozyczenie_url($id, $action) . '\'>' . $text . '</a>';
}

function query_wypozyczenie_all() {
    return 'SELECT * FROM mydb.wypozyczenie';
}

function query_wypozyczenie_by_id($id) {
    return 'SELECT * FROM mydb.wypozyczenie WHERE idwypozyczenie=' . $id;
}

function display_wypozyczenie_table() {
    $query = query_wypozyczenie_all();
    display_query_result($query, 'get_wypozyczenie_link');
}

function get_wypozyczenie_list($id, $name='') {
    $query = 'SELECT idwypozyczenie, data_transakcji, klient_id FROM mydb.wypozyczenie';
    $result = get_query_result($query);
    $drop_list = '<select name=\'' . $name . '\'>';
    $drop_list .= '<option value=\'\'> N/A </option>';
    while ($row = pg_fetch_row($result)) {
        $drop_list .= '<option value=\'' . $row[0] . '\'>' . $row[1] . ' - '. $row[2] . '</option>';
    }
    $drop_list .= '</select>';
    return $drop_list;

}

function display_wypozyczenie_entry($id, $for_edit) {
    if ($for_edit) {
        $map_dict['kon_id'] = 'get_kon_list';
        $map_dict['klient_id'] = 'get_klient_list';
    } else {
        $map_dict['kon_id'] = 'get_kon_link';
        $map_dict['klient_id'] = 'get_klient_link';
    }
    $query = query_wypozyczenie_by_id($id);
    display_entry($query, $map_dict, $for_edit);
}

?>
