<?
require_once('common.php');
require_once('kon_helpers.php');
require_once('klient_helpers.php');

function delete_sprzedaz_from_post() {
    $dbconn = connect_to_db();
    pg_delete($dbconn, 'mydb.sprzedaz', array('idsprzedaz' => $_POST['idsprzedaz']));
}

function insert_sprzedaz_from_post() {
    $dbconn = connect_to_db();
    pg_insert($dbconn, 'mydb.sprzedaz', $_POST);
}

function update_sprzedaz_from_post() {
    $dbconn = connect_to_db();
    pg_update($dbconn, 'mydb.sprzedaz', $_POST, array('idsprzedaz' => $_POST['idsprzedaz']));
}

function get_sprzedaz_url($id, $action='get') {
    return 'sprzedaz.php?id=' . $id . '&action=' . $action;
}

function get_sprzedaz_link($id, $action='get', $text = NULL) {
    if (is_null($text)) {
        $text = $id;
    }
    return '<a href=\'' . get_sprzedaz_url($id, $action) . '\'>' . $text . '</a>';
}

function query_sprzedaz_all() {
    return 'SELECT * FROM mydb.sprzedaz';
}

function query_sprzedaz_by_id($id) {
    return 'SELECT * FROM mydb.sprzedaz WHERE idsprzedaz=' . $id;
}

function display_sprzedaz_table() {
    $query = query_sprzedaz_all();
    display_query_result($query, 'get_sprzedaz_link');
}

function get_sprzedaz_list($id, $name='') {
    $query = 'SELECT idsprzedaz, data_transakcji, klient_id FROM mydb.sprzedaz';
    $result = get_query_result($query);
    $drop_list = '<select name=\'' . $name . '\'>';
    $drop_list .= '<option value=\'\'> N/A </option>';
    while ($row = pg_fetch_row($result)) {
        $drop_list .= '<option value=\'' . $row[0] . '\'>' . $row[1] . ' - '. $row[2] . '</option>';
    }
    $drop_list .= '</select>';
    return $drop_list;

}

function display_sprzedaz_entry($id, $for_edit) {
    if ($for_edit) {
        $map_dict['kon_id'] = 'get_kon_list';
        $map_dict['klient_id'] = 'get_klient_list';
    } else {
        $map_dict['kon_id'] = 'get_kon_link';
        $map_dict['klient_id'] = 'get_klient_link';
    }
    $query = query_sprzedaz_by_id($id);
    display_entry($query, $map_dict, $for_edit);
}

?>
