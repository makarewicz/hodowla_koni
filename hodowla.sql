﻿DROP SCHEMA IF EXISTS "mydb" CASCADE;
CREATE SCHEMA "mydb";
SET search_path TO mydb,public;

-- -----------------------------------------------------
-- Table "mydb"."pracownik"
-- -----------------------------------------------------
DROP TABLE IF EXISTS "mydb"."pracownik" ;

CREATE  TABLE IF NOT EXISTS "mydb"."pracownik" (
  "idpracownik" SERIAL ,
  "pesel" VARCHAR(11) NOT NULL ,
  "imie" VARCHAR(45) NOT NULL ,
  "nazwisko" VARCHAR(45) NOT NULL ,
  "czy_weterynarz" BOOLEAN NOT NULL ,
  "czy_treser" BOOLEAN NOT NULL ,
  "czy_dozorca" BOOLEAN NOT NULL ,
  PRIMARY KEY ("idpracownik") );


-- -----------------------------------------------------
-- Table "mydb"."kon"
-- -----------------------------------------------------
DROP TABLE IF EXISTS "mydb"."kon" ;

CREATE  TABLE IF NOT EXISTS "mydb"."kon" (
  "idkon" SERIAL ,
  "imie" VARCHAR(45) NOT NULL ,
  "data_urodzenia" DATE NOT NULL ,
  "ojciec_id" INT NULL ,
  "matka_id" INT NULL ,
  "treser_id" INT NULL ,
  "czy_samiec" BOOLEAN NOT NULL ,
  PRIMARY KEY ("idkon") );
CREATE INDEX ojciec_index ON kon (ojciec_id);
CREATE INDEX matka_index ON kon (matka_id);
CREATE INDEX treser_index ON kon (treser_id);


-- -----------------------------------------------------
-- Table "mydb"."box"
-- -----------------------------------------------------
DROP TABLE IF EXISTS "mydb"."box" ;

CREATE  TABLE IF NOT EXISTS "mydb"."box" (
  "idbox" SERIAL ,
  "kon_id" INT NULL ,
  PRIMARY KEY ("idbox") ,
  CONSTRAINT "zajmuje"
    FOREIGN KEY ("kon_id" )
    REFERENCES "mydb"."kon" ("idkon" )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
CREATE INDEX zajmuje_index ON box (kon_id);


-- -----------------------------------------------------
-- Table "mydb"."sprzatanie"
-- -----------------------------------------------------
DROP TABLE IF EXISTS "mydb"."sprzatanie" ;

CREATE  TABLE IF NOT EXISTS "mydb"."sprzatanie" (
  "idsprzatanie" SERIAL ,
  "box_id" INT NOT NULL ,
  "dozorca_id" INT NOT NULL ,
  "data_sprzatania" TIMESTAMP NOT NULL ,
  PRIMARY KEY ("idsprzatanie") ,
  CONSTRAINT "sprzatany"
    FOREIGN KEY ("box_id" )
    REFERENCES "mydb"."box" ("idbox" )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT "sprzatajacy"
    FOREIGN KEY ("dozorca_id" )
    REFERENCES "mydb"."pracownik" ("idpracownik" )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
CREATE INDEX sprzatany_index ON sprzatanie (box_id);
CREATE INDEX sprzatajacy_index ON sprzatanie (dozorca_id);


-- -----------------------------------------------------
-- Table "mydb"."klient"
-- -----------------------------------------------------
DROP TABLE IF EXISTS "mydb"."klient" ;

CREATE  TABLE IF NOT EXISTS "mydb"."klient" (
  "idklient" SERIAL ,
  "pesel" VARCHAR(11) NOT NULL ,
  "imie" VARCHAR(45) NOT NULL ,
  "nazwisko" VARCHAR(45) NOT NULL ,
  "nr_telefonu" VARCHAR(15) NOT NULL ,
  PRIMARY KEY ("idklient") );


-- -----------------------------------------------------
-- Table "mydb"."sprzedaz"
-- -----------------------------------------------------
DROP TABLE IF EXISTS "mydb"."sprzedaz" ;

CREATE  TABLE IF NOT EXISTS "mydb"."sprzedaz" (
  "idsprzedaz" SERIAL ,
  "cena" INT NOT NULL ,
  "data_transakcji" TIMESTAMP NOT NULL ,
  "klient_id" INT NOT NULL ,
  "kon_id" INT NOT NULL ,
  PRIMARY KEY ("idsprzedaz") ,
  CONSTRAINT "obiekt_sprzedazy"
    FOREIGN KEY ("kon_id" )
    REFERENCES "mydb"."kon" ("idkon" )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT "kupujacy"
    FOREIGN KEY ("klient_id" )
    REFERENCES "mydb"."klient" ("idklient" )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
CREATE INDEX obiekt_sprzedazy_index ON sprzedaz (kon_id);
CREATE INDEX kupujacy_index ON sprzedaz (klient_id);


-- -----------------------------------------------------
-- Table "mydb"."zabieg"
-- -----------------------------------------------------
DROP TABLE IF EXISTS "mydb"."zabieg" ;

CREATE  TABLE IF NOT EXISTS "mydb"."zabieg" (
  "idzabieg" SERIAL ,
  "opis_zabiegu" TEXT NOT NULL ,
  "kon_id" INT NOT NULL ,
  "weterynarz_id" INT NOT NULL ,
  PRIMARY KEY ("idzabieg") ,
  CONSTRAINT "obiekt_zabiegu"
    FOREIGN KEY ("kon_id" )
    REFERENCES "mydb"."kon" ("idkon" )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT "weterynarz"
    FOREIGN KEY ("weterynarz_id" )
    REFERENCES "mydb"."pracownik" ("idpracownik" )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
CREATE INDEX obiekt_zabiegu_index ON zabieg (kon_id);
CREATE INDEX weterynarz_index ON zabieg (weterynarz_id);


-- -----------------------------------------------------
-- Table "mydb"."kupno"
-- -----------------------------------------------------
DROP TABLE IF EXISTS "mydb"."kupno" ;

CREATE  TABLE IF NOT EXISTS "mydb"."kupno" (
  "idkupno" SERIAL ,
  "cena" INT NOT NULL ,
  "data_transakcji" TIMESTAMP NOT NULL ,
  "klient_id" INT NOT NULL ,
  "kon_id" INT NOT NULL ,
  PRIMARY KEY ("idkupno") ,
  CONSTRAINT "obiekt_kupna"
    FOREIGN KEY ("kon_id" )
    REFERENCES "mydb"."kon" ("idkon" )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT "sprzedajacy"
    FOREIGN KEY ("klient_id" )
    REFERENCES "mydb"."klient" ("idklient" )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
CREATE INDEX obiekt_kupna_index ON kupno (kon_id);
CREATE INDEX sprzedajacy_index ON kupno (klient_id);

-- -----------------------------------------------------
-- Table "mydb"."wypozyczenie"
-- -----------------------------------------------------
DROP TABLE IF EXISTS "mydb"."wypozyczenie" ;

CREATE  TABLE IF NOT EXISTS "mydb"."wypozyczenie" (
  "idwypozyczenie" SERIAL ,
  "cena" INT NOT NULL ,
  "data_transakcji" TIMESTAMP NOT NULL ,
  "klient_id" INT NOT NULL ,
  "kon_id" INT NOT NULL ,
  "poczatek_wypozyczenia" TIMESTAMP NOT NULL ,
  "planowany_koniec_wypozyczenia" TIMESTAMP NOT NULL ,
  "koniec_wypozyczenia" TIMESTAMP NULL ,
  PRIMARY KEY ("idwypozyczenie") ,
  CONSTRAINT "obiekt_wypozyczenia"
    FOREIGN KEY ("kon_id" )
    REFERENCES "mydb"."kon" ("idkon" )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT "wypozyczajacy"
    FOREIGN KEY ("klient_id" )
    REFERENCES "mydb"."klient" ("idklient" )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
CREATE INDEX obiekt_wypozyczenia_index ON wypozyczenie (kon_id);
CREATE INDEX wypozyczajacy_index ON wypozyczenie (klient_id);


-- -----------------------------------------------------
-- Logger
-- Source: http://wiki.postgresql.org/wiki/Audit_trigger
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS "audit" CASCADE;
CREATE SCHEMA "audit";


CREATE TABLE audit.logged_actions (
  "idlog" SERIAL,
  "schema_name" TEXT NOT NULL,
  "table_name" TEXT NOT NULL,
  "user_name" TEXT,
  "action_tstamp_utc" TIMESTAMP NOT NULL DEFAULT (now() AT TIME ZONE 'UTC'),
  "action" TEXT NOT NULL CHECK (action IN ('I','D','U')),
  "original_data" TEXT,
  "new_data" TEXT,
  "query" TEXT
);
CREATE INDEX logged_actions_schema_table_idx ON audit.logged_actions(((schema_name||'.'||table_name)::TEXT));
CREATE INDEX logged_actions_action_tstamp_utc_idx ON audit.logged_actions(action_tstamp_utc);
CREATE INDEX logged_actions_action_idx ON audit.logged_actions(action);
 
 
-- generic function for all tables
DROP FUNCTION IF EXISTS audit.if_modified_func() CASCADE;
CREATE OR REPLACE FUNCTION audit.if_modified_func() RETURNS TRIGGER AS $body$
DECLARE
  v_old_data TEXT;
  v_new_data TEXT;
BEGIN
  IF (TG_OP = 'UPDATE') THEN
    v_old_data := ROW(OLD.*);
    v_new_data := ROW(NEW.*);
    INSERT INTO audit.logged_actions (schema_name,table_name,user_name,action,original_data,new_data,query)
      VALUES (TG_TABLE_SCHEMA::TEXT,TG_TABLE_NAME::TEXT,session_user::TEXT,substring(TG_OP,1,1),v_old_data,v_new_data, current_query()); 
    RETURN NEW;
  ELSIF (TG_OP = 'DELETE') THEN
    v_old_data := ROW(OLD.*);
    INSERT INTO audit.logged_actions (schema_name,table_name,user_name,action,original_data,query)
      VALUES (TG_TABLE_SCHEMA::TEXT,TG_TABLE_NAME::TEXT,session_user::TEXT,substring(TG_OP,1,1),v_old_data, current_query());
    RETURN OLD;
  ELSIF (TG_OP = 'INSERT') THEN
    v_new_data := ROW(NEW.*);
    INSERT INTO audit.logged_actions (schema_name,table_name,user_name,action,new_data,query)
      VALUES (TG_TABLE_SCHEMA::TEXT,TG_TABLE_NAME::TEXT,session_user::TEXT,substring(TG_OP,1,1),v_new_data, current_query());
    RETURN NEW;
  ELSE
    RAISE WARNING '[AUDIT.IF_MODIFIED_FUNC] - Other action occurred: %, at %',TG_OP,now();
    RETURN NULL;
  END IF;
 
EXCEPTION
  WHEN data_exception THEN
    RAISE WARNING '[AUDIT.IF_MODIFIED_FUNC] - UDF ERROR [DATA EXCEPTION] - SQLSTATE: %, SQLERRM: %',SQLSTATE,SQLERRM;
    RETURN NULL;
  WHEN unique_violation THEN
    RAISE WARNING '[AUDIT.IF_MODIFIED_FUNC] - UDF ERROR [UNIQUE] - SQLSTATE: %, SQLERRM: %',SQLSTATE,SQLERRM;
    RETURN NULL;
  WHEN OTHERS THEN
    RAISE WARNING '[AUDIT.IF_MODIFIED_FUNC] - UDF ERROR [OTHER] - SQLSTATE: %, SQLERRM: %',SQLSTATE,SQLERRM;
    RETURN NULL;
END;
$body$ language plpgsql security definer;

-- -----------------------------------------------------
-- Triggers
-- -----------------------------------------------------

-- Activating logger for every table
CREATE TRIGGER pracownik_if_modified_trg BEFORE INSERT OR UPDATE OR DELETE ON pracownik FOR EACH ROW EXECUTE PROCEDURE audit.if_modified_func();
CREATE TRIGGER kon_if_modified_trg BEFORE INSERT OR UPDATE OR DELETE ON kon FOR EACH ROW EXECUTE PROCEDURE audit.if_modified_func();
CREATE TRIGGER box_if_modified_trg BEFORE INSERT OR UPDATE OR DELETE ON box FOR EACH ROW EXECUTE PROCEDURE audit.if_modified_func();
CREATE TRIGGER sprzatanie_if_modified_trg BEFORE INSERT OR UPDATE OR DELETE ON sprzatanie FOR EACH ROW EXECUTE PROCEDURE audit.if_modified_func();
CREATE TRIGGER klient_if_modified_trg BEFORE INSERT OR UPDATE OR DELETE ON klient FOR EACH ROW EXECUTE PROCEDURE audit.if_modified_func();
CREATE TRIGGER sprzedaz_if_modified_trg BEFORE INSERT OR UPDATE OR DELETE ON sprzedaz FOR EACH ROW EXECUTE PROCEDURE audit.if_modified_func();
CREATE TRIGGER zabieg_if_modified_trg BEFORE INSERT OR UPDATE OR DELETE ON zabieg FOR EACH ROW EXECUTE PROCEDURE audit.if_modified_func();
CREATE TRIGGER kupno_if_modified_trg BEFORE INSERT OR UPDATE OR DELETE ON kupno FOR EACH ROW EXECUTE PROCEDURE audit.if_modified_func();
CREATE TRIGGER wypozyczenie_if_modified_trg BEFORE INSERT OR UPDATE OR DELETE ON wypozyczenie FOR EACH ROW EXECUTE PROCEDURE audit.if_modified_func();


-- -----------------------------------------------------
-- Functions
-- -----------------------------------------------------

DROP FUNCTION IF EXISTS mydb.boxy_wymagajace_sprzatniecia() CASCADE;

CREATE FUNCTION mydb.boxy_wymagajace_sprzatniecia() RETURNS SETOF mydb.box AS $$
  SELECT box FROM mydb.box LEFT JOIN mydb.sprzatanie
    ON mydb.box.idbox = mydb.sprzatanie.box_id AND mydb.sprzatanie.data_sprzatania > (current_date - 3)
      where mydb.sprzatanie.idsprzatanie IS NULL;
$$ LANGUAGE sql;


/*
-- -----------------------------------------------------
-- Quick test
-- -----------------------------------------------------
INSERT INTO box VALUES (1, NULL);
INSERT INTO box VALUES (2, NULL);
INSERT INTO pracownik VALUES (1, '12345678901', 'jan', 'kowalski', FALSE, TRUE, TRUE);
INSERT INTO sprzatanie VALUES (1, 1, 1, CURRENT_TIMESTAMP);
INSERT INTO kon VALUES(1, 'Marco', '1991-09-25', NULL, NULL, 1, true);
UPDATE kon SET imie='Apollo' WHERE idkon=1;
*/
