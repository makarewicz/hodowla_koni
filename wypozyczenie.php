<?php
include('nav.php');
require_once('wypozyczenie_helpers.php');
if (isset($_POST['usun'])) {
    delete_wypozyczenie_from_post();
} else if (!isset($_POST['idwypozyczenie']) && isset($_POST['cena'])) {
    insert_wypozyczenie_from_post();
} else if(isset($_POST['cena'])) {
    update_wypozyczenie_from_post();
}
if (!isset($_GET['action'])) {
    echo get_wypozyczenie_link(-1, 'edit', 'Dodaj nowe wypozyczenie');
    display_wypozyczenie_table();
} else if ($_GET['action'] == 'get') {
    display_wypozyczenie_entry($_GET['id'], false);
} else if ($_GET['action'] == 'edit') {
    echo '<form action="wypozyczenie.php" method="post">';
    display_wypozyczenie_entry($_GET['id'], true);
    echo '<input type="submit" value="Wyslij" />';
    echo '</form>';

    if($_GET['id'] > 0) {
        echo "<form action='wypozyczenie.php' method='post'>";
        echo "<input type='hidden' name='idwypozyczenie' value='" . $_GET['id'] . "' />";
        echo "<input type='hidden' name='usun' value='true' />";
        echo "<input type='submit' value='USUN' />";
        echo "</form>";
    }
    

}
?>
