<?
require_once('common.php');
require_once('kon_helpers.php');
require_once('klient_helpers.php');

function delete_kupno_from_post() {
    $dbconn = connect_to_db();
    pg_delete($dbconn, 'mydb.kupno', array('idkupno' => $_POST['idkupno']));
}

function insert_kupno_from_post() {
    $dbconn = connect_to_db();
    pg_insert($dbconn, 'mydb.kupno', $_POST);
}

function update_kupno_from_post() {
    $dbconn = connect_to_db();
    pg_update($dbconn, 'mydb.kupno', $_POST, array('idkupno' => $_POST['idkupno']));
}

function get_kupno_url($id, $action='get') {
    return 'kupno.php?id=' . $id . '&action=' . $action;
}

function get_kupno_link($id, $action='get', $text = NULL) {
    if (is_null($text)) {
        $text = $id;
    }
    return '<a href=\'' . get_kupno_url($id, $action) . '\'>' . $text . '</a>';
}

function query_kupno_all() {
    return 'SELECT * FROM mydb.kupno';
}

function query_kupno_by_id($id) {
    return 'SELECT * FROM mydb.kupno WHERE idkupno=' . $id;
}

function display_kupno_table() {
    $query = query_kupno_all();
    display_query_result($query, 'get_kupno_link');
}

function get_kupno_list($id, $name='') {
    $query = 'SELECT idkupno, data_transakcji, klient_id FROM mydb.kupno';
    $result = get_query_result($query);
    $drop_list = '<select name=\'' . $name . '\'>';
    $drop_list .= '<option value=\'\'> N/A </option>';
    while ($row = pg_fetch_row($result)) {
        $drop_list .= '<option value=\'' . $row[0] . '\'>' . $row[1] . ' - '. $row[2] . '</option>';
    }
    $drop_list .= '</select>';
    return $drop_list;

}

function display_kupno_entry($id, $for_edit) {
    if ($for_edit) {
        $map_dict['kon_id'] = 'get_kon_list';
        $map_dict['klient_id'] = 'get_klient_list';
    } else {
        $map_dict['kon_id'] = 'get_kon_link';
        $map_dict['klient_id'] = 'get_klient_link';
    }
    $query = query_kupno_by_id($id);
    display_entry($query, $map_dict, $for_edit);
}

?>
