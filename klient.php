<?php
include('nav.php');
require_once('klient_helpers.php');
if (isset($_POST['usun'])) {
    delete_klient_from_post();
} else if (!isset($_POST['idklient']) && isset($_POST['imie'])) {
    insert_klient_from_post();
} else if(isset($_POST['imie'])) {
    update_klient_from_post();
}
if (!isset($_GET['action'])) {
    echo get_klient_link(-1, 'edit', 'Dodaj nowego klienta');
    display_klient_table();
} else if ($_GET['action'] == 'get') {
    display_klient_entry($_GET['id'], false);
} else if ($_GET['action'] == 'edit') {
    echo '<form action="klient.php" method="post">';
    display_klient_entry($_GET['id'], true);
    echo '<input type="submit" value="Wyslij" />';
    echo '</form>';

    if($_GET['id'] > 0) {
        echo "<form action='klient.php' method='post'>";
        echo "<input type='hidden' name='idklient' value='" . $_GET['id'] . "' />";
        echo "<input type='hidden' name='usun' value='true' />";
        echo "<input type='submit' value='USUN' />";
        echo "</form>";
    }
    

}
?>
