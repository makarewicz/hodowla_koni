<?
require_once('common.php');

function delete_pracownik_from_post() {
    $dbconn = connect_to_db();
    pg_delete($dbconn, 'mydb.pracownik', array('idpracownik' => $_POST['idpracownik']));
}

function insert_pracownik_from_post() {
    $dbconn = connect_to_db();
    pg_insert($dbconn, 'mydb.pracownik', $_POST);
}

function update_pracownik_from_post() {
    $dbconn = connect_to_db();
    pg_update($dbconn, 'mydb.pracownik', $_POST, array('idpracownik' => $_POST['idpracownik']));
}

function get_pracownik_url($id, $action='get') {
    return 'pracownik.php?id=' . $id . '&action=' . $action;
}

function get_pracownik_link($id, $action='get', $text = NULL) {
    if (is_null($text)) {
        $text = $id;
    }
    return '<a href=\'' . get_pracownik_url($id, $action) . '\'>' . $text . '</a>';
}

function query_pracownik_all() {
    return 'SELECT * FROM mydb.pracownik';
}

function query_pracownik_by_id($id) {
    return 'SELECT * FROM mydb.pracownik WHERE idpracownik=' . $id;
}

function display_pracownik_table() {
    $query = query_pracownik_all();
    display_query_result($query, 'get_pracownik_link');
}

function get_pracownik_list($id, $name='') {
    $query = 'SELECT idpracownik, imie FROM mydb.pracownik';
    $result = get_query_result($query);
    $drop_list = '<select name=\'' . $name . '\'>';
    $drop_list .= '<option value=\'\'> N/A </option>';
    while ($row = pg_fetch_row($result)) {
        $drop_list .= '<option value=\'' . $row[0] . '\'>' . $row[1] . '</option>';
    }
    $drop_list .= '</select>';
    return $drop_list;

}

function display_pracownik_entry($id, $for_edit) {
    $map_dict = array();
    $query = query_pracownik_by_id($id);
    display_entry($query, $map_dict, $for_edit);
}

?>
