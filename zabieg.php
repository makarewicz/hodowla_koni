<?php
include('nav.php');
require_once('zabieg_helpers.php');
if (isset($_POST['usun'])) {
    delete_zabieg_from_post();
} else if (!isset($_POST['idzabieg']) && isset($_POST['kon_id'])) {
    insert_zabieg_from_post();
} else if(isset($_POST['kon_id'])) {
    update_zabieg_from_post();
}
if (!isset($_GET['action'])) {
    echo get_zabieg_link(-1, 'edit', 'Dodaj nowego zabiegia');
    display_zabieg_table();
} else if ($_GET['action'] == 'get') {
    display_zabieg_entry($_GET['id'], false);
} else if ($_GET['action'] == 'edit') {
    echo '<form action="zabieg.php" method="post">';
    display_zabieg_entry($_GET['id'], true);
    echo '<input type="submit" value="Wyslij" />';
    echo '</form>';

    if($_GET['id'] > 0) {
        echo "<form action='zabieg.php' method='post'>";
        echo "<input type='hidden' name='idzabieg' value='" . $_GET['id'] . "' />";
        echo "<input type='hidden' name='usun' value='true' />";
        echo "<input type='submit' value='USUN' />";
        echo "</form>";
    }
    

}
?>
