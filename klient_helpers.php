<?
require_once('common.php');

function delete_klient_from_post() {
    $dbconn = connect_to_db();
    pg_delete($dbconn, 'mydb.klient', array('idklient' => $_POST['idklient']));
}

function insert_klient_from_post() {
    $dbconn = connect_to_db();
    pg_insert($dbconn, 'mydb.klient', $_POST);
}

function update_klient_from_post() {
    $dbconn = connect_to_db();
    pg_update($dbconn, 'mydb.klient', $_POST, array('idklient' => $_POST['idklient']));
}

function get_klient_url($id, $action='get') {
    return 'klient.php?id=' . $id . '&action=' . $action;
}

function get_klient_link($id, $action='get', $text = NULL) {
    if (is_null($text)) {
        $text = $id;
    }
    return '<a href=\'' . get_klient_url($id, $action) . '\'>' . $text . '</a>';
}

function query_klient_all() {
    return 'SELECT * FROM mydb.klient';
}

function query_klient_by_id($id) {
    return 'SELECT * FROM mydb.klient WHERE idklient=' . $id;
}

function display_klient_table() {
    $query = query_klient_all();
    display_query_result($query, 'get_klient_link');
}

function get_klient_list($id, $name='') {
    $query = 'SELECT idklient, imie FROM mydb.klient';
    $result = get_query_result($query);
    $drop_list = '<select name=\'' . $name . '\'>';
    $drop_list .= '<option value=\'\'> N/A </option>';
    while ($row = pg_fetch_row($result)) {
        $drop_list .= '<option value=\'' . $row[0] . '\'>' . $row[1] . '</option>';
    }
    $drop_list .= '</select>';
    return $drop_list;

}

function display_klient_entry($id, $for_edit) {
    $map_dict = array();
    $query = query_klient_by_id($id);
    display_entry($query, $map_dict, $for_edit);
}

?>
