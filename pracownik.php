<?php
include('nav.php');
require_once('pracownik_helpers.php');
if (isset($_POST['usun'])) {
    delete_pracownik_from_post();
} else if (!isset($_POST['idpracownik']) && isset($_POST['imie'])) {
    insert_pracownik_from_post();
} else if(isset($_POST['imie'])) {
    update_pracownik_from_post();
}
if (!isset($_GET['action'])) {
    echo get_pracownik_link(-1, 'edit', 'Dodaj nowego pracownika');
    display_pracownik_table();
} else if ($_GET['action'] == 'get') {
    display_pracownik_entry($_GET['id'], false);
} else if ($_GET['action'] == 'edit') {
    echo '<form action="pracownik.php" method="post">';
    display_pracownik_entry($_GET['id'], true);
    echo '<input type="submit" value="Wyslij" />';
    echo '</form>';

    if($_GET['id'] > 0) {
        echo "<form action='pracownik.php' method='post'>";
        echo "<input type='hidden' name='idpracownik' value='" . $_GET['id'] . "' />";
        echo "<input type='hidden' name='usun' value='true' />";
        echo "<input type='submit' value='USUN' />";
        echo "</form>";
    }
    

}
?>
