<?
require_once('common.php');
require_once('pracownik_helpers.php');
require_once('wypozyczenie_helpers.php');

function delete_kon_from_post() {
    $dbconn = connect_to_db();
    pg_delete($dbconn, 'mydb.kon', array('idkon' => $_POST['idkon']));
}

function insert_kon_from_post() {
    $dbconn = connect_to_db();
    pg_insert($dbconn, 'mydb.kon', $_POST);
}

function update_kon_from_post() {
    $dbconn = connect_to_db();
    pg_update($dbconn, 'mydb.kon', $_POST, array('idkon' => $_POST['idkon']));
}

function get_kon_url($id, $action='get') {
    return 'kon.php?id=' . $id . '&action=' . $action;
}

function get_kon_link($id, $action='get', $text = NULL) {
    if (is_null($text)) {
        $text = $id;
    }
    return '<a href=\'' . get_kon_url($id, $action) . '\'>' . $text . '</a>';
}

function query_kon_all() {
    return 'SELECT * FROM mydb.kon';
}

function query_kon_by_id($id) {
    return 'SELECT * FROM mydb.kon WHERE idkon=' . $id;
}

function display_kon_table() {
    $query = query_kon_all();
    display_query_result($query, 'get_kon_link');
}

function display_kon_wypozyczenia($id) {
    $query = 'SELECT * FROM mydb.wypozyczenie WHERE kon_id=' . $id . ' ORDER BY data_transakcji DESC';
    display_query_result($query, 'get_wypozyczenie_link');
}


function get_kon_list($id, $name='') {
    $query = 'SELECT idkon, imie FROM mydb.kon';
    $result = get_query_result($query);
    $drop_list = '<select name=\'' . $name . '\'>';
    $drop_list .= '<option value=\'\'> N/A </option>';
    while ($row = pg_fetch_row($result)) {
        $drop_list .= '<option value=\'' . $row[0] . '\'>' . $row[1] . '</option>';
    }
    $drop_list .= '</select>';
    return $drop_list;

}

function display_kon_entry($id, $for_edit) {
    if ($for_edit) {
        $map_dict['treser_id'] = 'get_pracownik_list';
        $map_dict['ojciec_id'] = 'get_kon_list';
        $map_dict['matka_id'] = 'get_kon_list';
    } else {
        $map_dict['treser_id'] = 'get_pracownik_link';
        $map_dict['ojciec_id'] = 'get_kon_link';
        $map_dict['matka_id'] = 'get_kon_link';
    }
    $query = query_kon_by_id($id);
    display_entry($query, $map_dict, $for_edit);
}

?>
